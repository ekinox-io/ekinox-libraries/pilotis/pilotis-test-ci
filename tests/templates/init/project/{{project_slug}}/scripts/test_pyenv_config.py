import shutil
from pathlib import Path
from subprocess import run

from hamcrest import assert_that, matches_regexp

import pilotis

PYENV_CONFIG_WITH_PYENV_MOCK_SCRIPT = "pyenv_config_with_pyenv_mock.bash"


def test_pyenv_config_bash_script_should_select_target_version_given_already_defined_in_parent_directory(  # noqa: E501
    tmp_path: Path,
):
    # Given
    _create_python_dir(tmp_path)
    test_script_path = _create_script_dir(tmp_path)

    _put_mock_pyenv_config_bash_script_into_test_path(test_script_path=test_script_path)
    _put_pyenv_config_bash_script_into_test_path(
        test_script_path=test_script_path,
    )

    # When
    output = _run_script_in_dir(test_script_path / PYENV_CONFIG_WITH_PYENV_MOCK_SCRIPT)

    # Then
    stdout = str(output.stdout)
    assert_that(stdout, matches_regexp("Marking .* to use Python 3.7.9"))


def _run_script_in_dir(script_path: Path):
    script_dir_path = script_path.parent.absolute()
    script_name = script_path.name
    return run(
        f"cd {script_dir_path} && ./{script_name}",
        capture_output=True,
        shell=True,
    )


def _create_python_dir(tmp_path: Path):
    python_path = tmp_path / "python"
    python_path.mkdir()


def _create_script_dir(tmp_path: Path) -> Path:
    test_script_path = tmp_path / "scripts"
    test_script_path.mkdir()
    return test_script_path


def _put_mock_pyenv_config_bash_script_into_test_path(test_script_path: Path):
    parent_dir = Path(__file__).parent
    shutil.copyfile(
        parent_dir / PYENV_CONFIG_WITH_PYENV_MOCK_SCRIPT,
        test_script_path / PYENV_CONFIG_WITH_PYENV_MOCK_SCRIPT,
    )
    run(
        f"cd {test_script_path} && chmod +x {PYENV_CONFIG_WITH_PYENV_MOCK_SCRIPT}",
        shell=True,
    )


def _put_pyenv_config_bash_script_into_test_path(test_script_path: Path):
    project_root_path = Path(pilotis.__file__).parents[1]
    template_path = project_root_path / "templates"
    real_pyenv_config_bash = (
        template_path
        / "init"
        / "project"
        / "{{project_slug}}"
        / "scripts"
        / "pyenv_config.bash"
    )
    test_pyenv_config_bash = test_script_path / "pyenv_config.bash"
    shutil.copyfile(real_pyenv_config_bash, test_pyenv_config_bash)
