import re

_SLUG_FORBIDDEN_CHAR = "[_ ]"


def slugify(project_name: str) -> str:
    lowered_name = project_name.lower()
    return re.sub(_SLUG_FORBIDDEN_CHAR, "-", lowered_name)
