# Data organization

## Landing zone

It is structured as follows:

```
landing
|_ raw
| |_ <dataset_name>
|   |_ <dataset_version>
|     |_ dataset_file_1
|     |_ dataset_file_2
|_ parsed
  |_ <dataset_name>
    |_ <dataset_version>
      |_ dataset_file_1
      |_ dataset_file_2
```

The `raw` folder contains the data in the same format as delivered by external applications.
It is structured by dataset, each of them having possibly multiple versions, and multiple files for each version.

The `parsed` folder has the same structure as the `raw` folder, but with files in a format optimized for reading:
some schema has been applied, and the data may be compressed.

### Use cases zone

It has the following structure:

```
use_cases
|_ <use_case_name>
  |_ datasets
  | |_ <dataset_version>
  |   |_ <dataset_name>
  |     |_ dataset_file
  |_ models
  | |_ <model_name>
  |   |_ <model_run_id>
  |     |_ model_file_1
  |     |_ model_file_2
  |_ exports
    |_ <export_name>
      |_ <export_run_id>
        |_ export_file_1
        |_ export_file_2
```

The `datasets` folder contains datasets which have been generated for this specific use case.
For example, it could contain a dataset with some feature engineering, ready for applying a ML model.
The `name` and `version` levels are reversed in comparison to the `landing` zone because multiple dataset may be generated for the same `version` of the application.

The `models` folder contains all the generated ML models. Each model can be trained multiple times, so each we can see that each `model_name` can contain multiple `run_id`.

The `export` folder contains all datasets and objects to export for each use case, usually generated by the ML models.
Just as for the `models` folder, you will find first the `name` notion, and the the `run_id` notion.
